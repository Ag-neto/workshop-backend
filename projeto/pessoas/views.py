from django.views.generic import  ListView, CreateView, DeleteView
from django.urls import reverse_lazy
from .models import Pessoa
from .forms import PessoaForm

#LOCAL ONDE É A PONTE ENTRE OS MODELOS (BANCO DE DAOS) SE COMUNICARÁ COM
#PODENDO HAVER LÓGICA COMO VALIDACAO DOS DADOS. PARA INICIAR O SERVER
# python manage.py runserver

def listar_pessoas(request):
    pessoas = Pessoa.objects.all()
    return render(request, 'pessoas/listar_pessoas.html', { 'pessoas': pessoas})

class PessoaListView(ListView):
    model = Pessoa
    template_name = 'pessoas/listar_pessoas.html'
    context_object_name = 'pessoas'

class PessoaCreateView(CreateView):
    model = Pessoa
    template_name = 'pessoas/criar_pessoa.html'
    form_class = PessoaForm
    success_url = reverse_lazy('pessoas_pessoa_list')

class PessoaDeleteView(DeleteView):
    model = Pessoa
    success_url = reverse_lazy('pessoas_pessoa_list')






