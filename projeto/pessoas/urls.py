from django.urls import path
from .views import PessoaListView, PessoaCreateView, PessoaDeleteView

# CAMINHOS DA URL (PÁGINA/SITE A IR) RELACIONADAS AS VIEWS

# 127.0.0.1:8000/pessoas/criar/

# 123.0.0.1:8000/pessoas/[IDENTIFICACAO UNICA DA PESSOA - ID]/deletar

# PK -> PRIMARY KEY -> IDENTIFICADOR DO MODELO (PESSOA)

urlpatterns = [
    path('', PessoaListView.as_view(), name='pessoas_pessoa_list'),
    path('criar/', PessoaCreateView.as_view(), name='pessoas_pessoa_create'),
    path('<int:pk>/delete', PessoaDeleteView.as_view(), name='pessoas_pessoa_delete')
]
